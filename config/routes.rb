Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  post '/result' => 'payments#result'
  post '/payments' => 'payments#index'
  resources :transaction

  get '/transactions', to: 'transactions#index', as: 'transactions'
  root 'payments#index'
  post 'payments/validate'

  # get '/login' => 'login# index'
  # get '/users' => 'users# new'
  get '/login' => 'sessions#new'
  get '/auth/:provider/callback' => 'sessions#create'
  get 'auth/failure', to: redirect('/')
  delete 'signout', to: 'sessions#destroy', as: 'signout'
end
