# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171031043219) do

  create_table "data_inputs", force: :cascade do |t|
    t.string "trax_type", limit: 20
    t.string "invoice", limit: 25
    t.float "amount"
    t.string "add_info1", limit: 30
    t.string "add_info2", limit: 30
    t.string "add_info3", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_expireds", force: :cascade do |t|
    t.string "trax_type", limit: 20
    t.string "mer_signature", limit: 64
    t.string "merchant_id", limit: 12
    t.string "invoice", limit: 22
    t.string "payment_code", limit: 20
    t.string "result_code", limit: 4
    t.string "result_desc", limit: 50
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_successes", force: :cascade do |t|
    t.string "trax_type", limit: 20
    t.string "mer_signature", limit: 64
    t.string "merchant_id", limit: 12
    t.string "invoice", limit: 22
    t.float "amount"
    t.string "payment_code", limit: 20
    t.string "result_code", limit: 4
    t.string "result_desc", limit: 50
    t.string "log_no", limit: 20
    t.string "payment_source", limit: 20
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string "invoice"
    t.integer "harga"
    t.text "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "requests", force: :cascade do |t|
    t.string "trax_type", limit: 20
    t.string "mer_signature", limit: 64
    t.string "merchant_id", limit: 12
    t.string "invoice", limit: 25
    t.float "amount"
    t.string "add_info1", limit: 30
    t.string "add_info2", limit: 30
    t.string "add_info3", limit: 30
    t.string "add_info4", limit: 30
    t.string "add_info5", limit: 30
    t.decimal "timeout"
    t.string "return_url", limit: 512
    t.string "success_url", limit: 512
    t.string "failed_url", limit: 512
    t.decimal "timestamp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "responses", force: :cascade do |t|
    t.string "mer_signature", limit: 64
    t.string "trax_type", limit: 20
    t.string "va_name", limit: 30
    t.string "payment_code", limit: 20
    t.string "invoice", limit: 25
    t.string "result_code", limit: 4
    t.string "merchant_id", limit: 12
    t.string "redirect_url", limit: 250
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
