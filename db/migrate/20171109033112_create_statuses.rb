class CreateStatuses < ActiveRecord::Migration[5.1]
  def change
    create_table :statuses do |t|
      t.integer :invoice, default: 0
      t.datetime :time, null: false
      t.string :method, default: '-'
      t.string :event, default: '-'

      t.timestamps
    end
  end
end
