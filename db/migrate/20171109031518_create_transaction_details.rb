class CreateTransactionDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :transaction_details, id: false, primary_key: :invoice  do |t|
      t.integer :invoice, default: 0
      t.string :name, default: '-'
      t.string :address, default: '-'
      t.integer :phone, default: '0000000000'
      t.string :email, null: false
      t.string :products, default: '-'
      t.string :detail, default: '-'
      t.string :status, default: '-'

      t.timestamps
    end
  end
end
