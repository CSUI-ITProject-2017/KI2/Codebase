class ChangeIntegerLimitInTransactionDetails < ActiveRecord::Migration[5.1]
  def change
  	change_column :transaction_details, :phone, :integer, limit: 8
  end
end
