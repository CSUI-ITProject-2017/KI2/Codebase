class CreateTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions, id: false, primary_key: :invoice do |t|
      t.datetime :time, null: false
      t.integer :invoice, default: 0
      t.string :source, default: '-'
      t.string :method, default: '-'
      t.string :subject, default: '-'
      t.integer :amount, default: 0
      t.string :status, default: '-'

      t.timestamps
    end
  end
end
