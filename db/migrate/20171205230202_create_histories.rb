class CreateHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :histories do |t|
      t.integer :invoice
      t.text :text

      t.timestamps
    end
  end
end
