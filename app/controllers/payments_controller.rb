require_relative 'finnet_connect'
require 'json'

class PaymentsController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def validate
    @transaksi = Transaksi.new(time: Time.now, invoice: params[:invoice], source: params[:source],
                               method: params[:trax_type], subject: params[:add_info4],
                               amount: params[:amount], status: 'WAITING_SUBJECT')
    @detail = TransactionDetail.new(invoice: params[:invoice], name: params[:add_info1],
                                    address: params[:add_info2], phone: params[:add_info3], email: params[:add_info4],
                                    products: params[:desc], detail: '-', status: 'WAITING_SUBJECT')
    @time = Time.now
    @text = "#{@time.strftime('%H:%M:%S')} Customer Choose #{params[:trax_type]}"
    @history = History.new(invoice: params[:invoice], text: @text)

    if @transaksi.save
      if @detail.save
        @history.save
      	data = hash_maker_main(@transaksi, @detail)
        data['mer_signature'] = hash_data(upper_data(data))
        post = send_post('https://sandbox.finpay.co.id/servicescode/api/apiFinpay.php', data)

        if post == 'ECONNRESET'
    	 	 render 'err_reset'
         @transaksi.destroy
         @history.destroy
         @detail.destroy
        else
          post = JSON.parse(post)
          if @transaksi[:method] == 'finpay195' || @transaksi[:method] == 'finpay021'
    		  	@code = post['payment_code']
            if post['status_desc'] == 'Success'
    			   	@status = 'Waiting for Payment'
    			  else
    				  @status = post['status_desc']
    			  end 
            render 'code'
          elsif @transaksi[:method] == 'tcash'
            url = post['redirect_url']
            redirect_to url
          else
            render plain: post
          end
        end
			else
		  	@transaksi.destroy
		  end
	  end

  end

  def result
	  @time = Time.now
    if @transaksi = Transaksi.find_by(invoice: params['invoice'])
      if @detail = TransactionDetail.find_by(invoice: params['invoice'])
        if params['result_code'] == '00'
      		@transaksi[:status] = 'PAID'
      		@detail[:status] = 'PAID'
      		@text = "#{@time.strftime('%H:%M:%S')} Payment has been made"
      		@history = History.new(invoice: params['invoice'], text: @text)
        else
      		@transaksi[:status] = 'REJECTED'
      		@detail[:status] = 'REJECTED'
      		@text = "#{@time.strftime('%H:%M:%S')} Payment Failed"
      		@history = History.new(invoice: params['invoice'], text: @text)
    	  end
      	@transaksi.save
      	@detail.save
      	@history.save
      end
    end
  end

  def index
    if params.key?(:invoice) && params.key?(:desc) && params.key?(:amount) && params.key?(:source)
      @source = params[:source]
      @invoice = params[:invoice]
      @desc = params[:desc]
      @amount = params[:amount]
    else
      @invoice = 153_813_51
      @desc = 'Barang 5 1 3'
      @amount = 121_231_3
      @source = 'Peentar'
    end
  end

end
