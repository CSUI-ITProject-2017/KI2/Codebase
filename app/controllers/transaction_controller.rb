class TransactionController < ApplicationController
  def index
    @transactions = Transaksi.all
  end

  def show
    @transaction = TransactionDetail.find_by(invoice: params[:id])
    @histories = History.where(invoice: params[:id])
  end
end
