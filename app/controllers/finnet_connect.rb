require 'digest'
require 'net/http'
require 'date'

def hash_data(data)
  result = Digest::SHA256.hexdigest(data)
  return result
end

def send_post(url, data)
	begin
		postData = Net::HTTP.post_form(URI.parse(url), data)
	rescue Errno::ECONNRESET => e
		postData = "ECONNRESET"
		return postData
	end
	return postData.body
end

def upper_data(data)
  secret_key = 'KF2017'
  string = ''
  data.each do |key, value|
    if value.to_s != ''
      string << value.to_s << '%'
	end
  end
  string = string << secret_key
  return string.upcase
end

def hash_maker_main(transaksi, detail)
	data = Hash.new
	
	data['add_info1'] = detail[:name] 
	data['add_info2'] = detail[:address]
	data['add_info3'] = detail[:phone]
	data['add_info4'] = detail[:email]
	data['amount'] = transaksi[:amount]
	data['failed_url'] = 'http://paybuddy.herokuapp.com/result'
	data['invoice'] = detail[:invoice]
	data['items'] = "%5B%5B%22chicken%22,100,1%5D%5D" # Items Test for T Cash
	data['merchant_id'] = 'KF820'
	data['return_url'] = 'http://paybuddy.herokuapp.com/result'
	data['sof_id'] = transaksi[:method]
	data['sof_type'] = 'pay'
	data['success_url'] = 'http://paybuddy.herokuapp.com/result'
	data['timeout'] = 100
	data['trans_date'] = transaksi[:time].strftime("%Y%m%d%H%M%S") 
	data['mer_signature'] = ''
	return data
end

def hash_maker_sc(merchant_id, payment_code)
	data = Hash.new
	
	data['mer_signature'] = ''
	data['merchant_id'] = merchant_id
	data['payment_code'] = payment_code
	return data
end
