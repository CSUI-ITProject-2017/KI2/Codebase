# This is class History
class History < ApplicationRecord
  validates :invoice, presence: true
  validates :text, presence: true
end
