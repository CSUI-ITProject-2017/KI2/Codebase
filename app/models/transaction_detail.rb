class TransactionDetail < ApplicationRecord
	validates :invoice, presence: true
	validates :name, presence: true, format: { with: /\A[a-zA-Z ]+\z/, message: "only allows text" }

	validates :address, presence: true, format: { with: /\A[A-Za-z0-9., ]{5,30}\z/, message: "only allows text" }
	validates :phone, presence: true, format: { with: /\A[0-9]{5,15}\z/, message: "only allows number" }
	validates :email, presence: true, format: { with: /\A([A-Za-z0-9._-]{1,20}\@)+([A-Za-z0-9._-]{1,20})+\.([A-Za-z0-9._-]{1,5})+\z/, message: "only allows correct email"}
	validates :products, presence: true
	validates :detail, presence: true
	validates :status, presence: true
end
