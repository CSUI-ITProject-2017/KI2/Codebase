class Transaksi < ApplicationRecord
	validates :time, presence: true
	validates :source, presence: true
	validates :method, presence: true
	validates :subject, presence: true, format: { with: /\A([A-Za-z0-9._-]{1,20}\@)+([A-Za-z0-9._-]{1,20})+\.([A-Za-z0-9._-]{1,5})+\z/, message: "only allows correct email"}
	validates :amount, presence: true, numericality: { only_integer: true }
	validates :status, presence: true
end
