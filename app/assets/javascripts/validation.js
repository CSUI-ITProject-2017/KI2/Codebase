function validate(data, regex, id_err){
	if (regex.test(data)){
		document.getElementById(id_err).innerHTML = "";
		document.getElementById(id_err).value = "true";
	}
	else{
		document.getElementById(id_err).innerHTML = "Please input the right format";
		document.getElementById(id_err).value = "false";
	}
	check_v();
}

function check_v(){
	if(document.getElementById("name_err_msg").value=="true"&&document.getElementById("address_err_msg").value=="true"&&document.getElementById("phone_err_msg").value=="true"&&document.getElementById("email_err_msg").value=="true"){
		var radios = document.getElementById("main_form").elements["trax_type"];
		var len = radios.length;
		for(var i=0; i<len; i++){
			if(radios[i].checked){
				document.getElementById("submit").disabled = false;
				break;
			}
		}
	}
	else{
		document.getElementById("submit").disabled = true;
	}
}
