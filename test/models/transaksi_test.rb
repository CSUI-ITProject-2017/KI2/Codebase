require 'test_helper'

class TransaksiTest < ActiveSupport::TestCase
  test 'fail test no input' do
    td = Transaksi.new
    assert_not td.save
  end
  test 'fail test amount not numeric' do
    td = Transaksi.new(time:  '2017-11-09 09:54:50', source:  'Peentar', method:  'OmniChannel',
                       subject:  'test@test.com', amount: 'dua ribu', status:  'PENDING')
    assert_not td.save
  end
  test 'success test correct input' do
    td = Transaksi.new(time: '2017-11-09 09:54:50', source: 'Peentar', method: 'OmniChannel',
                       subject: 'test@test.com', amount: 120_000, status: 'PENDING')
    assert td.save
  end
end
