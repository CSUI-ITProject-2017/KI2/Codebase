require 'test_helper'

class HistoryTest < ActiveSupport::TestCase
  test 'fail test no input' do
    td = History.new
    assert_not td.save
  end
  test 'fail test wrong text input' do
    td = History.new(invoice: '123123', text: 'User choose to pay with BCA')
    assert td.save
  end
end
