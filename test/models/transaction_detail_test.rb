require 'test_helper'

class TransactionDetailTest < ActiveSupport::TestCase
  test 'fail test no input' do
    td = TransactionDetail.new
    assert_not td.save
  end

  test 'fail test wrong name input' do
    td = TransactionDetail.new(invoice: '123123', name: 'Hell00', address: 'Jl. Fasilkom 1',
                               phone: '081289057483', email: 'test@test.com',
                               products: 'Barang 1 3 5', detail: '-', status: 'PENDING')
    assert_not td.save
  end
  test 'fail test wrong address input' do
    td = TransactionDetail.new(invoice: '123123', name: 'Helloo', address: 'Jl. Fas!lkom 1',
                               phone: '081289057483', email: 'test@test.com',
                               products: 'Barang 1 3 5', detail: '-', status: 'PENDING')
    assert_not td.save
  end
  test 'fail test wrong phone input' do
    td = TransactionDetail.new(invoice: '123123', name: 'Helloo', address: 'Jl. Fasilkom 1',
                               phone: 'invalid01', email: 'test@test.com',
                               products: 'Barang 1 3 5', detail: '-', status: 'PENDING')
    assert_not td.save
  end
  test 'fail test wrong email input' do
    td = TransactionDetail.new(invoice: '123123', name: 'Helloo', address: 'Jl. Fasilkom 1',
                               phone: '0812901481', email: 'test.com',
                               products: 'Barang 1 3 5', detail: '-', status: 'PENDING')
    assert_not td.save
  end
  test 'success test correct input' do
    td = TransactionDetail.new(invoice: '123123', name: 'Helloo', address: 'Jl. Fasilkom 1',
                               phone: '0812901481', email: 'test@test.com',
                               products: 'Barang 1 3 5', detail: '-', status: 'PENDING')
    assert td.save
  end
end
