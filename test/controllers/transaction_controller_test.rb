require 'test_helper'

class TransactionControllerTest < ActionDispatch::IntegrationTest
  test 'should get transaction table' do
    get transaction_index_url
    assert_response :success
  end

  test 'should show transaction detail' do
    td = TransactionDetail.new(invoice: '123123', name: 'Helloo', address: 'Jl. Fasilkom 1',
                               phone: '0812901481', email: 'test@test.com',
                               products: 'Barang 1 3 5', detail: '-', status: 'PENDING')
    td.save
    get '/transaction/123123'
    assert_response :success
  end
end
