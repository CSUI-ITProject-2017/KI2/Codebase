require 'test_helper'

class PaymentsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index without params' do
    get '/'
    assert_response :success
  end

  test 'should get index with params' do
    get '/', params: { invoice: '123123', source: 'Peentar', desc: 'Barang 1 3 5',
                       amount: '120000' }
    assert_response :success
  end

  test 'success validate normal' do
    post payments_validate_url, params: { invoice: '123123', source: 'Peentar',
                                          trax_type: 'BNI_VA', add_info4: 'test@test.com',
                                          amount: '120000', add_info1: 'test',
                                          add_info2: 'Jl. Fasilkom 1', add_info3: '0812809410',
                                          desc: 'Barang 1 3 5', detail: '-' }
    assert_response :success
  end

  test 'failed result' do
    t = Transaksi.new(invoice: '123123', time: '2017-11-09 09:54:50', source: 'Peentar', method: 'OmniChannel',
                      subject: 'test@test.com', amount: 120_000, status: 'PENDING')
    td = TransactionDetail.new(invoice: '123123', name: 'Helloo', address: 'Jl. Fasilkom 1',
                               phone: '0812901481', email: 'test@test.com',
                               products: 'Barang 1 3 5', detail: '-', status: 'PENDING')
    t.save
    td.save
    post result_url, params: { invoice: '123123', result_code: '01' }
    assert_response :success
  end

  test 'success result' do
    t = Transaksi.new(invoice: '123123', time: '2017-11-09 09:54:50', source: 'Peentar', method: 'OmniChannel',
                      subject: 'test@test.com', amount: 120_000, status: 'PENDING')
    td = TransactionDetail.new(invoice: '123123', name: 'Helloo', address: 'Jl. Fasilkom 1',
                               phone: '0812901481', email: 'test@test.com',
                               products: 'Barang 1 3 5', detail: '-', status: 'PENDING')
    t.save
    td.save
    post result_url, params: { invoice: '123123', result_code: '00' }
    assert_response :success
  end

  test 'fail validate input detail' do
    post payments_validate_url, params: { invoice: '123123', source: 'Peentar',
                                          trax_type: 'BNI_VA', add_info4: 'test@test.com',
                                          amount: '120000', add_info1: 'test',
                                          add_info2: 'Jl. Fasilkom 1', add_info3: '0812809410',
                                          detail: '-' }
    assert_response :success
  end

  test 'fail validate input transaksi' do
    post payments_validate_url, params: { invoice: '123123', source: 'Peentar',
                                          trax_type: 'BNI_VA', add_info4: 'test.com',
                                          amount: '120000', add_info1: 'test',
                                          add_info2: 'Jl. Fasilkom 1', add_info3: '0812809410',
                                          desc: 'Barang 1 3 5', detail: '-' }
    assert_response :success
  end
end
