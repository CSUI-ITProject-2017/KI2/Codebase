# README
  
App Name : Paybuddy  
License : Proprietary software  
Release Date : 16-12-2017  
Ruby Version for this app : 2.4.3  
Database : postgresql  
System Dependencies : All dependencies are listed on Gemfile  
  
  
The Guide is tested for debian 9 xfce (https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.3.0-amd64-xfce-CD-1.iso)   
Note 1: Please configure the debian repository first before moving on with the guide, change the repository from cd rom to debian us repository on /etc/apt/source.list then run sudo apt-get update  
Note 2: if sudo is not installed by default please follow the guide from the link below:  
https://www.privateinternetaccess.com/forum/discussion/18063/debian-8-1-0-jessie-sudo-fix-not-installed-by-default  

Instruction for installing ruby
1. download Ruby 2.4.3 source code (https://cache.ruby-lang.org/pub/ruby/2.4/ruby-2.4.3.tar.gz)
2. extract the file (tar xf ruby-2.4.3.tar.gz)
3. go to the folder that has been extracted (cd ruby-2.4.3)
4. install debian base (sudo apt-get install build-essential)
5. install zlib1g-dev (sudo apt-get install zlib1g-dev)
6. install libgnutls-openssl27 (sudo apt-get install libgnutls-openssl27)
7. install libssl-dev libreadline-dev libgdbm-dev (sudo apt-get install libssl-dev libreadline-dev libgdbm-dev)
8. run ./configure
9. run sudo make
10. run sudo make install
11. check ruby version to make sure the version is 2.4.3(ruby --version)

Instruction for install and deploy paybuddy
1. install Ruby 2.4.3 (follow install Ruby section)
2. update gem (sudo gem update --system)
3. install git (sudo apt-get install git)
4. install bundler (sudo gem install bundler)
5. install postgres (sudo apt-get install postgresql postgresql-client)
6. install nodejs (sudo apt-get install nodejs)
7. install libpq-dev (sudo apt-get install libpq-dev)
8. git clone https://gitlab.com/CSUI-ITProject-2017/KI2/Codebase.git
9. go to Codebase folder (cd Codebase) 
10. run sudo bundler install
11. go to bin folder (cd bin)
12. enter postgresql interface (sudo -u postgres psql)
13. run CREATE USER paybuddy WITH PASSWORD '12345';
14. run ALTER ROLE paybuddy WITH CREATEDB;
15. run ALTER ROLE paybuddy WITH SUPERUSER;
16. quit postgresql interface (\q or CTR+D)
17. open pg_hba.conf file (sudo nano /etc/postgresql/9.6/main/pg_hba.conf) Note: postgresql installed in the system for the tutorial is 9.6
18. Change 'peer' word in line 90  into 'md5'
19. exit from text editor
20. restart postgresql (sudo /etc/init.d/postgresql restart)
21. run sudo rails db:setup
22. run sudo rails db:migrate
23. Congrat!!, paybuddy app can be run (sudo rails s)

Guide for sending data to paybuddy  
protocol:POST  
url: http://(domain name)/payments => example: http://localhost:3000/payments  


| Name        | Type           | example   |
| ------------|:--------------:| ---------:|
| source      | string         | "Peentar" |
| invoice     | string         |  "2134b"  |
| desc        | string         | "Packet 1"|
| amount      | int            |   2000    |
| locale      | string         |   "en"    |

for locale data:  
'en' => english  
'id' => indonesia  
